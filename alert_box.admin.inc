<?php

/**
 * Module settings form.
 */
function alert_box_settings() {
  $form['alert_box_enabled'] = array(
    '#type' => 'radios',
    '#title' => t('Alert Box Status'),
    '#default_value' => variable_get('alert_box_enabled', 0),
    '#options' => array(t('Disabled'), t('Enabled')),
    '#description' => t('When set to "Disabled", no message will display. When set to "Enabled", all pages on the site will display the alert message that is specified below. The alert message will only show if the "Alert Box" block is in a visible region of the site\'s theme as set on the <a href="@blocks-url">blocks</a> page.', array('@blocks-url' => url('admin/structure/block'))),
  );
  
  $alert_default = variable_get('alert_box_message', '');
  $form['alert_box_message'] = array(
    '#type' => 'text_format',
    '#title' => t('Alert Box Message'),
    '#default_value' => isset($alert_default['value']) ? $alert_default['value'] : '',
    '#format' => isset($alert_default['format']) ? $alert_default['format'] : NULL,
    '#description' => t('Message to show visitors when the alert box is enabled.'),
  );

  return system_settings_form($form);
}
